#!/usr/bin/env sh

rm -rf public/* -r

cd chapters

total_count=$(ls | wc -l)
count=1


echo "[" > ../_data/chapters.json

for i in $(ls);
do
	data=$(jq ". | {title: .title, description: .description, url: \"$i\"}" "$i/info.json")
	if [ "$total_count" -gt "$count" ];
	then
		echo "$data," >> ../_data/chapters.json
	else
		echo "$data" >> ../_data/chapters.json
	fi
	count=$(( $count + 1))
done

echo "]" >> ../_data/chapters.json


for comic in $(ls);
do
	cd "$comic"

	echo "---" > index.md
	echo "title: \"$(jq -r .title info.json)\"" >> index.md
	echo "---" >> index.md

	echo "# $(jq -r .title info.json)" >> index.md
	jq -r .description info.json >> index.md
	echo "" >> index.md

	echo "<div class=\"imgs-container\">" >> index.md
	for img_path in $(ls pages);
	do
		echo "<img src=\"pages/$img_path\" >" >> index.md
	done
	echo "</div>" >> index.md

	cd ..
done

cd ..
npx eleventy build
