module.exports = function(eleventyConfig) {
	eleventyConfig.addPassthroughCopy("**/*.jpg");
	eleventyConfig.addPassthroughCopy("**/*.png");
	eleventyConfig.addPassthroughCopy("**/*.svg");
	eleventyConfig.addPassthroughCopy("**/*.css");

	return {
		dir: {
			output: "public"
		}
	}
}
